public class StudentRecordStaticExample {
	public static void main(String[] args) {
		StudentRecordStatic anna = new StudentRecordStatic();
		StudentRecordStatic elsa = new StudentRecordStatic();
		StudentRecordStatic olaf = new StudentRecordStatic();
		StudentRecordStatic karyono = new StudentRecordStatic ("Karyono");
		StudentRecordStatic songhjung = new StudentRecordStatic ("Song H-Jung","Cibaduyut");
		StudentRecordStatic masbejo = new StudentRecordStatic (80,90,100);

		anna.setName("Anna");
		elsa.setName("Elsa");
		olaf.setName("Olaf");

		System.out.println(anna.getName());

		System.out.println("Count = "+StudentRecordStatic.getStudentCount());
		StudentRecordStatic hannah = new StudentRecordStatic();
		hannah.setName("Hannah");
		hannah.setAddress("Pinoy");
		hannah.setAge(15);
		hannah.setMathGrade(80);
		hannah.setEnglishGrade(100);
	
		hannah.print(hannah.getName());
		hannah.print(hannah.getEnglishGrade(),hannah.getMathGrade(),hannah.getScienceGrade());
		System.out.println(anna.getName());
		System.out.println(elsa.getName());
		System.out.println(olaf.getName());


	}
}