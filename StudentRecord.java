public class StudentRecord {
	public String name;
	public String address;
	public int age;
	public double mathGrade;
	public double englishGrade;
	public double scienceGrade;
	public double average;
	public static int studentCount;
	
	public StudentRecord() {
		//Inisialisasi
	}

	public StudentRecord(String temp) {
		this.name = temp;
	}

	public String getName() {
		return name;
	}

	public void setName(String temp) {
		name = temp;
		studentCount++;
	}
	
	public double getAverage(){
		double result = 0;
		result = (mathGrade+englishGrade+scienceGrade)/3;
		return result;
	}
	
	public static int getStudentCount() {
		return studentCount;
	}
}